package main

import "fmt"

func main(){
   x := [3]int {1,2,3}
   for i, v := range x{
       fmt.Printf("ind %d, val %d", i, v)
   }
   x2 := [...]int {4,8,5}
   y := x2[0:2]
   z := x2[1:3]
   fmt.Println(y)
   fmt.Println(z)
   y[0] = 1
   z[1] = 3
  fmt.Print(x2)
}