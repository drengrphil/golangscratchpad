package main

import (
        "fmt"
        "strings"
        "bufio"
        "io"
        "os"
)

func main(){
    // String variable to store user input.
    var userString string
    // Get array of runes
    var stringLine []rune
    // Store the runes
    var charIn rune
    var err error
    var stdin *bufio.Reader
    stdin = bufio.NewReader(os.Stdin)

    // Wait for user input.
    fmt.Printf("Please enter a string > ")
    // Capture user input including spaces.    
    for{
           charIn, _, err = stdin.ReadRune()
           if err == io.EOF || charIn == '\n' { break }
           if err != nil {
               fmt.Fprintf(os.Stderr, "Error reading standard input")
               os.Exit(1)
           }
           stringLine = append(stringLine, charIn)
    }

    // Convert the runes to string
    userString = string(stringLine[:len(stringLine)])

    switch{
        case strings.HasPrefix(userString, "i") && strings.Contains(userString, "a") && strings.HasSuffix(userString, "n"):
            fmt.Println("Found!")
        case strings.HasPrefix(userString, "I") && strings.Contains(userString, "a") && strings.HasSuffix(userString, "N"):
            fmt.Println("Found!")
        case strings.HasPrefix(userString, "I") && strings.Contains(userString, "a") && strings.HasSuffix(userString, "n"):
            fmt.Println("Found!")
        case strings.HasPrefix(userString, "i") && strings.Contains(userString, "a") && strings.HasSuffix(userString, "N"):
            fmt.Println("Found!")
        default:
            fmt.Println("Not Found!")
    }
}