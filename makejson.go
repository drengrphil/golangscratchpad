package main

import (
          "encoding/json"
          "fmt"
          "os"
          "strings"
          "bufio"
)


func main(){
        fmt.Printf("Enter your name > ")
        userInput := bufio.NewReader(os.Stdin)
        userName, _ := userInput.ReadString('\n')
        userName = strings.TrimSpace(userName)

        fmt.Printf("Enter your address > ")
        userAddr, _ := userInput.ReadString('\n')
        userAddr = strings.TrimSpace(userAddr)

        // Create Map
        var userDetailsMap = make(map[string] string)
        userDetailsMap["name"] = userName
        userDetailsMap["address"] = userAddr

        // Convert the map to JSON.
        userDetailsJson, err := json.Marshal(userDetailsMap)
        // Check if error occured
        if err != nil{
            fmt.Println(err.Error())
        }
        fmt.Println("Data in Map format: ", userDetailsMap)

        // JSON reprsentation
        userData := string(userDetailsJson)
        fmt.Println("Your Data in JSON: ", userData)
}