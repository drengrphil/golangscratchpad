package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	a := getUserInput("Enter acceleration")
	v := getUserInput("Enter velocity")
	s0 := getUserInput("Enter initial displacement")
	t := getUserInput("Enter time")

	fmt.Printf("You entered a=%f, v=%f, s0=%f, t=%f \n", a, v, s0, t)

	displaceFn := GenDisplaceFn(a, v, s0)
	fmt.Printf("Final displacement is: %f \n", displaceFn(t))
}

func getUserInput(command string) float64 {
	var userInput string
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Printf("%s: ", command)

	scanner.Scan()
	userInput = strings.ToLower(scanner.Text())

	number, err := strconv.ParseFloat(userInput, 64)

	if err != nil {
		fmt.Println("Wrong number. Try again!")
		return getUserInput(command)
	}

	return number
}

func GenDisplaceFn(acceleration float64, velocity float64, initDisplacement float64) func(time float64) float64 {
	return func(time float64) float64 {
		return (acceleration*time*time)/2 + velocity*time + initDisplacement
	}
}
