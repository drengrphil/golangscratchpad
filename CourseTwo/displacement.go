package main

import (
        "fmt"
        "os"
        "bufio"
        "strings"
        "strconv"
        "math"
)

// Generate displacement function.
func GenDisplaceFn(acc float64, v_o float64, s_o float64)func(float64)float64{
     fn := func(time_t float64)float64{
         return 0.5*acc*math.Pow(time_t, 2) + v_o*time_t + s_o
     }
  return fn
}

func main(){
    // Acceleration
    fmt.Println("------------------------------------------------------")
    fmt.Println("Enter acceleration, initial velocity and displacement ")
    fmt.Println("------------------------------------------------------")
    fmt.Println("Separate each value by space, e.g. 2.0 3.5 5")
    fmt.Printf(" > ")
    userInputReader := bufio.NewReader(os.Stdin)
    inputValues, _ := userInputReader.ReadString('\n')
    inputValues = strings.TrimSpace(inputValues)
    inputVals := strings.Split(inputValues, " ")

    acceleration, _ := strconv.ParseFloat(inputVals[0], 2)
    velocity, _ := strconv.ParseFloat(inputVals[1], 2)
    displ, _ := strconv.ParseFloat(inputVals[2], 2)
    
    // Generate function to calculate displacement.
    fnDisplacement := GenDisplaceFn(acceleration, velocity, displ)
    
    // Displacement calculation.
    // Ask user for the elapsed time values
    fmt.Println("----------------------------------------------------")
    fmt.Println("Enter time steps separated by space, e.g. 0 1 2 3...")
    fmt.Printf(" > ")
    timeReader := bufio.NewReader(os.Stdin)
    timeValues, _ := timeReader.ReadString('\n')
    timeValues = strings.TrimSpace(timeValues)
    timeSteps := strings.Split(timeValues, " ")
    fmt.Println("----------------------------------------------------")

    // Calculate and print displacement for each time step.
    for k := range timeSteps{
       time_t, _ := strconv.ParseFloat(timeSteps[k], 2)
       fmt.Print("Time: ", time_t) 
       fmt.Println("    Displacement: ", fnDisplacement(time_t))
    }
}

// Problem:
// Let us assume the following formula for displacement s as a 
// function of time t, acceleration a, initial velocity v_o, and 
// initial displacement s_o.
//
// s = 1/2at^2 + v_ot + s_o
//
// Write a program which first prompts the user to enter values 
// for acceleration, initial velocity, and initial displacement. 
// Then the program should prompt the user to enter a value for 
// time and the program should compute the displacement after 
// the entered time.
//
// You will need to define and use a function called GenDisplaceFn() 
// which takes three float64 arguments, acceleration a, initial 
// velocity v_o, and initial displacement s_o. GenDisplaceFn() 
// should return a function which computes displacement as a 
// function of time, assuming the given values acceleration, 
// initial velocity, and initial displacement. The function returned
// by GenDisplaceFn() should take one float64 argument t, representing 
// time, and return one float64 argument which is the displacement 
// travelled after time t.