package main

import (
        "fmt"
        "os"
        "bufio"
        "strings"
)

// type defined data
type Animal struct{
   food string
   locomotion string
   noise string
}

func (ani *Animal) Eat(){
    fmt.Println(ani.food)
}

func (ani *Animal) Move(){
    fmt.Println(ani.locomotion)
}

func (ani *Animal) Speak(){
    fmt.Println(ani.noise)
}

func main(){
  for{
     // Acceleration
     fmt.Println("------------------------------------------------------")
     fmt.Println("Enter Animal and Information needed: e.g. cow speak   ")
     fmt.Println("------------------------------------------------------")
     fmt.Printf(" > ")
     userInputReader := bufio.NewReader(os.Stdin)
     inputValues, _ := userInputReader.ReadString('\n')
     inputValues = strings.TrimSpace(inputValues)
     inputVals := strings.Split(inputValues, " ")

     // Catch wrong user input
     if (len(inputVals) < 2){
         fmt.Println("Not Enough Input")
         fmt.Println("Enter Animal Name and Information separate by space")
         fmt.Println("")
         continue
     }

     // Name of animal
     animalName   := strings.ToLower(inputVals[0])
     // Information about the animal.    
     animalAction := strings.ToLower(inputVals[1])

     // Animal kinds and actions.
     cow := Animal{"grass", "walk", "moo"}
     bird := Animal{"worms", "fly", "peep"}
     snake := Animal{"mice", "slither", "hsss"}

     switch{
         case strings.Compare(animalName, "cow")==0:
              if (strings.Compare(animalAction, "eat")==0){
                  cow.Eat()
              }else if (strings.Compare(animalAction, "move")==0){
                  cow.Move()
              }else if (strings.Compare(animalAction, "speak")==0){
                  cow.Speak()
              }else{
                  fmt.Println("Unknown Information")
              }

         case strings.Compare(animalName, "bird")==0:
              if (strings.Compare(animalAction, "eat")==0){
      	         bird.Eat()
      	      }else if (strings.Compare(animalAction, "move")==0){
      		 bird.Move()
      	      }else if (strings.Compare(animalAction, "speak")==0){
      		 bird.Speak()
              }else{
                 fmt.Println("Unknown Information")
              }

         case strings.Compare(animalName, "snake")==0:
              if (strings.Compare(animalAction, "eat")==0){
                 snake.Eat()
              }else if (strings.Compare(animalAction, "move")==0){
                 snake.Move()
              }else if (strings.Compare(animalAction, "speak")==0){
                 snake.Speak()
              }else{
                 fmt.Println("Unknown Information")
              }

         default:
              fmt.Println("Unknown Animal or Action")
     }
  }   
}