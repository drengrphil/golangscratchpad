package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func readInput() []string {
	readInput := bufio.NewScanner(os.Stdin)
	fmt.Printf("> ")
	readInput.Scan()
	temp := readInput.Text()
	words := strings.Fields(temp)
	return words
}

// Animal type
type Animal struct {
	food       string
	locomotion string
	noise      string
}

// Eat Eating Function
func (a *Animal) Eat() {
	println(a.food)
}

// Move Moving Function
func (a *Animal) Move() {
	println(a.locomotion)
}

// Speak Speaking function
func (a *Animal) Speak() {
	println(a.noise)
}

func main() {

	cow := Animal{
		food:       "grass",
		locomotion: "walk",
		noise:      "moo",
	}
	bird := Animal{
		food:       "worms",
		locomotion: "fly",
		noise:      "peep",
	}
	snake := Animal{
		food:       "mice",
		locomotion: "slither",
		noise:      "hsss",
	}

	for {
		invalid := false

		words := readInput()
		animal := words[0]
		interaction := words[1]

		if animal == "cow" {
			if interaction == "speak" {
				cow.Speak()
			} else if interaction == "move" {
				cow.Move()
			} else if interaction == "eat" {
				cow.Eat()
			} else {
				invalid = true
			}
		} else if animal == "bird" {
			if interaction == "speak" {
				bird.Speak()
			} else if interaction == "move" {
				bird.Move()
			} else if interaction == "eat" {
				bird.Eat()
			} else {
				invalid = true
			}
		} else if animal == "snake" {
			if interaction == "speak" {
				snake.Speak()
			} else if interaction == "move" {
				snake.Move()
			} else if interaction == "eat" {
				snake.Eat()
			} else {
				invalid = true
			}
		} else {
			invalid = true
		}
		if invalid == true {
			fmt.Println("Invalid input. Please try again.")
		}
	}
}
