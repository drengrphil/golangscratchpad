package main

import (
        "fmt"
        "os"
        "bufio"
        "strings"
        "strconv"
)

// Swap operation to swap position of two adjacent
// elements in the slice.
func Swap(userDigits []int, indx int){
    tempPos := userDigits[indx]
    userDigits[indx] = userDigits[indx+1]
    userDigits[indx+1] = tempPos
}


// Bubble sort function to sort slice of integers
// in order.
func BubbleSort(userDigits []int){
    for k := 0; k < len(userDigits)-1; k++{
        for j := 0; j < len(userDigits)-k-1; j++{
            if userDigits[j] > userDigits[j+1]{
                Swap(userDigits, j)
            }
        }
    }
}

func main(){
    // Enter a sequence of upto 10 integers.
    fmt.Println("Enter sequence of upto 10 integers separated by space.")
    fmt.Printf(" > ")
    userInputReader := bufio.NewReader(os.Stdin)
    inputSequence, _ := userInputReader.ReadString('\n')
    inputSequence = strings.TrimSpace(inputSequence)

   // Grab the sequence and store it in slice.
   sliceInputIntSeq := make([]int, 0, 10)
   // Split the input separated by whitespace.
   inputSeq := strings.Split(inputSequence, " ")
   
   // Process user input and add each integer to slice.
   for k := range inputSeq{
      intVal, _ := strconv.Atoi(inputSeq[k]) 
      sliceInputIntSeq = append(sliceInputIntSeq, intVal)
   }

   // Perform bubble sort operation
   BubbleSort(sliceInputIntSeq)

   // Final sorted.
   fmt.Println("Sorted Sequence:")
   fmt.Println(sliceInputIntSeq)
}

// Write a Bubble Sort program in Go. The program should prompt the 
// user to type in a sequence of up to 10 integers. The program should 
// print the integers out on one line, in sorted order, from least to greatest. 
// Use your favorite search tool to find a description of how the bubble 
// sort algorithm works.

// As part of this program, you should write a function called BubbleSort() 
// which takes a slice of integers as an argument and returns nothing. 
// The BubbleSort() function should modify the slice so that the elements 
// are in sorted order.

// A recurring operation in the bubble sort algorithm is the Swap operation 
// which swaps the position of two adjacent elements in the slice. 
// You should write a Swap() function which performs this operation. 
// Your Swap() function should take two arguments, a slice of integers and 
// an index value i which indicates a position in the slice. The Swap() 
// function should return nothing, but it should swap the contents of the 
// slice in position i with the contents in position i+1.