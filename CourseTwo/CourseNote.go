// Functions, Methods, and Interfaces in Go
// Why Use Functions
// Function name is optional in Go.
// Function main is a special function and thats where execution starts.


// Why use a function:
//    Reusability - not rewriting same code over and over again.
//       Things that are used a lot can be placed inside a function.
//    Abstraction - grouping things and hiding the details. Organizing
//       complex stuffs.
//       Improves understandability and readability. Naming is important
         for functions to improve understandability.

// Function parameters and return values.
//   Function need data to work on, parameters; function operate the variables
//   internally. Sometimes no need for argument.
//   func foo(x int, y int){
//       fmt.Print(x*y)
//   }

// Function can also have output, a return value. The type of return value
//    must be part of the function declaration.
//    func foo(x int) int{
//       return x + 1
//    }
//
//    Function call:
//       y := foo(1)
//    Function returning multiple return values
//        func foo2(x int) (int, int){
//            return x, x + 1
//        }
//    Function call:
//        a, b := foo2(2)
//        a = 2, b = 3


// Call by value and reference
//   How arguments are passed to the function.
//   Go uses call by value - arguments passed to the function and copied
//   to parameters. The value is the copy and not the original value or variable.
//   modifying argument doesn't change the original variable.
//  Example:
//    func foo(y int){
//       y = y + 1
//    }
// Function call:
//    x := 2
//    foo(x)
//  x is not changed by foo because y just take a copy and not the original in
//  x.
// Advantage : Limits propagation of error (Data Encapsulation). Function errors
//   are localized.
// Disadvantage: Copying item. Large objects may take a long time to copy.

// Call by reference: not built into Go but could be done mannually.
//    Useful if we want the function to modify or alter the variable.
// Example:
//    func foo(y, *int){
//        *y = *y + 1
//    } 
//
//   FUnction call
//      x := 2
//      foo(&x)
//  func foo will alter x even though x is not defined in foo.
// Advantage: Copying time:...no need to copy arguments.
// Disadvantage: No data encapsulation: function variables may be changed in
// in called function.

// Passing Arrays and Slices to function
//    Array arguments are copied by value, so, they have to be copied.
//    Arrays can be big, so this can be a problem.
//
//    func foo(x [3]int) int{
//        return x[0]
//    }
//
//    func main(){
//       a := [3]int{1,2,3}
//       fmt.Print(foo(a))
//    }
//   
//    Instead of passing array, we can pass pointer array (pass by reference)
//    
//    func foo(x *[3]int) int{
//        (*x)[0] = (*x)[0] + 1
//    }
//
//    func main(){
//       a := [3]int{1,2,3}
//       foo(&a)
//       fmt.Print(a)
//   }
//  In Go, it is better to use slices (windows) on array.
//      Passing a slice copies the pointer because slice contains a pointer.
//
//  func foo(sli int) int{
//     sli[0] = sli[0] + 1
//  }
//
//  func main(){
//     a := []int{1,2,3}
//     foo(a)
//     fmt.Print(a)
//  }
//  In Go, when using array, pass the slice instead of the array.

// Well-written a function:
//    Understandability is key. Code is functions and data
//        To find a feature quickly. Understand the organization of your own code.
//        Easy to find things. Better if somebody can find functions.
//        Another thing, understanding where data is defined and used.
//    Debugging Principles:
//        Function is written incorrectly.
//        Or Data used by function is wrong.
//    Supporting Debugging:
//        Functions need to be understandable to determine function behavior.
//        Data needs to traceable to find source of problem.
//        Global variable add complication and the source of problem is hard
//        to trace.

// Guidelines for Functions:
//    1. Function naming - name that describes function behavior.
//           Function should related to application area.
//           Or domain dependent e.g. ComputeRMS
//    2. Parameters naming are important too.
//    3. Naming shouldn't be too long.
//    4. Functional cohesion - function should perform only one operation.
//       operation depends on the context.
//    5. Make them simpler and limit the number of parameters.
//       Makes it easy to trace the parameters.
//       Functionality can be separated to reduce numbers of parameters to pass.
//       Grouping related arguments into a struct.
//    6. Not too complicated:
//       To make them simple, write less, easier to debug
//       Limit the complexity of each function.
//       Use function call hierarchy to simply functions complexity.
//       Group functions in reasonable way.
//    7. Control-flow Complexity:
//       Start to the end of function - sequence of instructions
//       Complexity can be measured in terms of number of control-flow paths.
//       Separating the conditional flow.

// Functions are First-class
//    Means treating a function like any other type such as float, int ..
//       Creating function dynamically on the fly anywhere in the code
//       Pass function as an argument and retruned values
//       Function returning a new function as returned value
//       Function can be stored in data struct.
//    Meaning generally, a function can be treated as other kind of type
//    Variables as functions
//    var funcVar func(int) int
//    func incFn(x int) int {
//            return x + 1
//        } 
//
//    func main(){
//        // funcVar is merely a pointer to incFn
//        funcVar = incFn
//        fmt.Print(funcVar(1))
//    }
//
//   Function as arguments to another function
//   func applyIt(afunct func (int) int, val int) int{
//        return afunct(val)
//   }
//
//   func incFn(x int) int {return x + 1}
//   func decFn(x int) int {retirn x - 1}
//
//   func main(){
//       fmt.Println(applyIt(incFn, 2))
//       fmt.Println(applyIt(decFn, 2))
//   }
//
//   Anonymous Functions:
//      No need to name a function
//      Place function definition directly in the call.
//
//   func main(){
//       v := applyIt(func (x int) int{return x + 1}, 2)
//       fmt.Println(v)
//   }
//
//   Returning Functions as return values.
//       Why?: make new function that acts differently, that is
//             paramterizable. A function could create a new function
//             with new set of parameters. New function with different
//             properties.
//       Example: Distance to origin function:
//           Takes a point(x,y,coordinates)
//           Returns distance to origin
//           Create new function with new origin (if origin moves or
//           changes).
//       Exam: Origin location is passed as an argument
//             Origin is built into the retruned function.
//             This special function can be used to create multiple
//                functions
//       func MakeDistOrigin (o_x, o_y float64) func(float64, float64)
//          float64{
//             fn := func(x,y float64)float64{
//                     return math.Sqrt(math.Pow(x - o_x, 2) +
//                                     math.Pow(y - o_y, 2))
//                   }
//             return fn
//       }
//
//       func main(){
//            Dist1 := MakeDistOrigin(0,0)
//            Dist2 := MakeDistOrigin(2,2)
//            fmt.Println(Dist1(2,2))
//            fmt.Println(Dist2(2,2))
//            // Dist1 and Dist2 have different origins.
//       {
//
//    Environment of a Function
//       - Set of all names that are valid inside a function
//       - Names defined locally in the function
//       - Environment includes names defined in the block where the
//         function is defined
//
//   Closure: Function and its environment is called closure.
//      When a function is passed as argument, function still
//      come along with its enviornment.
//   When fn is called, o_x and o_y are still in the closure of fn()
//
//   Variadic and Deferred
//       Passing a number of arguments to function.
//       Function that takes variable number of arguments.
//   Use "..." (ellipses) to indicate variable number of arguments.
//
//   func getMax(vals ...int) int {
//        maxV := -1
//        for _, V := range vals{
//              if V > maxV{
//                  maxV = V
//              }
//        }
//     return maxV
//   }
//
//   fmt.Println(getMax(1, 3, 4, 5, 6))
//  Pass slice to getMax:
//   fmt.Println(getMax(vslice...))
//
//  Deferred function: they dont get executed when they are called
//  they get called until the surrounding function completes.
//    used for cleanup activities such as close opened files
//
//  func main(){
//    defer fmt.Println("Bye!")
//    fmt.Println("Hello")
//  }
//
//  Arguments of a deferred call are evaluated immediately.
//  And not later when the call happens.
//  
//  func main(){
//      i := 1
//      defer fmt.Println(i+1)     // i is already 2 here.
//      i++
//      fmt.Println("Hello!")
//  }
//
// Object-Oriented Programming
// 
//  Classes: collection of data fields and functions that shared
//    a well-defined responsibility.
//    Data and functions are all related.
//
//  Object: an instance of a class (instantiation).
//  Encapsulation: Hide data from the programmer - user of a class.
//    conceal something:
//     --> Data can be protected from the programmer.
//     --> Data can be accessed only using methods of the class.
//         To keep the data consistent.
//           That is, data can't be accessed from the outside.
// Support for classes:
//    Go doesn't have class keyword unlike other OO languages.
// Associating methods and data (Class):
//
// In Go, receiver type is used to define a class.
// Example:
//
// type MyInt int
//
// func (mi MyInt)Double() int {
//     return int(mi*2)
// }
//
// func main(){
//    v := MyInt(3)
//    fmt.Println(v.Double())
// }
//
// Class object with multiple Data fields:
//  Using struct as receiver type
//
// Associate Structs with Methods to get traditional features of classes.
// type Point struct{
//     x float64
//     y float64
// }
//
// func (p Point) DistToOrig(){
//    t := math.Pow(p.x, 2) + math.Pow(p.y, 2)
//    return math.Sqrt(t)
// }
//
// func main(){
//     p1 := Point(3,4)
//     fmt.Println(p1.DistToOrig())
// }
//
// Encapsulation
//    keeping private data.
// hide it but keep controlled access to it.
// Define public functions to allow external package access the data.
//
// package data
// var x int = 1
// func PrintX(){fmt.Println(x)}
//
// package main
// import "data"
// func main(){
//     data.PrintX()
// }
//
// Controlling access to structs
//
// type Point struct{
//     x float64
//     y float64
// }
//
// The following method are public because they are started with cap letter.
//
// func (p *Point) InitMe(xn, yn float64){
//     p.x = xn
//     p.y = yn
// }
//
// func (p *Point) Scale(v float64){
//     p.x = p.x * v
//     p.y = p.y * v
// }
//
// func (p *Point) PrintMe(){
//     fmt.Println(p.x, p.y)
// }
//
// package main
// func main(){
//    var p data.Point
//    p.InitMe(3,4)
//    p.Scale(2)
//    p.PrintMe()
// }
//
// Pointer Receivers
//
// Limitations of non-pointer receivers:
//    Receiver is passed implicitly as an argument to the method.
//    Method cannot modify the data inside the receiver
//    Nonpointer receiver usese call by value, which copies data
//
// Using Pointer receivers to overcome these limitations.
//
// func (p *Pointer) OffsetX(v float64){
//     p.x = p.x + v
// }
// Receiver can be a pointer to a type
// Call by reference, pointer is passed to the method.
// p.x points to the actual x value in memory and can now be modified.
//
// With Pointer receivers, no need to dereference
//    p.x = p.x + v // No need to say *(p.x) = *(p.x) + v
// Go understands what we meant and just dereference for us authomatically.
//
// No need to reference
//
// func main(){
//     p := Point(3,4)
//     p.OffsetX(5)
//     fmt.Println(p.x) // No need to reference using & when calling the
//       method.
// }
//
//
// Polymorphism
//
// Property commonly associated with OOP.
// An object have different forms depending on the context.
//
// Same method name doing different things:
//   Area() computes:
//      1. Area of a rectangle, area = base * height
//      2. Area of a triangle, area = 0.5*base*height
// They are high level of abstraction - doing the same thing.
// At low level, they are different as they do compute differently.
//
// One method doing different things. In tranditional languages
// 1. Inheritance is used.
// Subclass inherits the methods/data of the superclass.
//
// Example:
//
// Speaker superclass: Speak() method, prints "<noise>"
//
// Cat and Dog are different forms of speaker
//
// Go does not have inheritance.
//
// Overriding:
// Subclass redefines a method inherited from the superclass.
// Example:
//   Speaker (superclass), Cat, Dog
//   Speak() prints "<noise>"
//   Cat Speak() prints "meow"   // Redefine Speak() method to do diff thing
//   Dog Speak() prints "woof"   // Redefine Speak() method to do diff thing
//
// Therefore, Speak() is polymorphic: 
//   different implementations for each class.
//
// The two Speak() methods in Cat and Dog has the same signature:
//    name, parameters, and return type.
//
// Interface: Use in Go to achieve polymorphisms. Set of method signatures
// Used to express conceptually similarity between types
// Shape2D interface - Area() and Perimeter()
//
// Type satifies an interface if type defines all methods specified
// in the interface: provided same method signatures.
//
// Defining an interface type
//
// type Shape2D interface{
       Area() float64
       Perimeter() float64
// }
//
// type Triangle{...}
//
// func (t Triangle) Area() float64{...}
// func (t Triangle) Perimeter() float64{...}
//
// Interface vs Concrete Types
//
// Fundamentally different:
// Concrete types: specify the exact representation of data and methods.
//   Complete method implementation is included.
// Interface Types: No data is specified and only specify method signatures
//   Methods implementations are abstracted.
//   Eventually get mapped to a concrete type.
//
// Interface Values
// Can be treated like other values.
//    Assigned to variables
//    Passed, returned.
//
// Interface values have two components:
//   1. Dynamic Type:  Concrete type which it is assigned to
//   2. Dynamic Value: value of the dynamic type

// Concrete vs Interface Types:
//
// Concrete Types:
//   - Specify the exact representation of the data and methods.
//   - Complete method implementation is included
//
// Interface Types:
//   - Specifies some method signatures.
//   - Implementations are abstracted.
//
// Interface gets mapped to a concrete type.
//
// Interface values components:
//   1. Dynamic Type: concrete type which it is assigned to.
//   2. Dynamic Value: value of the dynamic type
//
//   // Interface definition
//   type Speaker interface { Speak()}
//
//   // Structure type
//   type Dog struct{name string}
//   
//   // Make struct Dog satisfies Speaker interface 
//   func (d Dog) Speak(){
//       fmt.Println(d.name)
//   }
//   
//   //
//   func main(){
//       var s1 Speaker
//       // Concrete type
//       var d1 Dog{"Brian"}
//       // Concrete type assigned to interface s1
//       // Dynamic type of s1 is Dog type.
//       // Dynamic value of s1 is d1, which contains "Brain"
//       s1 = d1
//       s1.Speak()
//       // Interface has dynamic type and dynamic value
//   }
//
// Interface with Nil Dynamic value
//   Having a dynamic type but a nil dynamic value
//
//   var s1 Speaker
//   var d1 *Dog // d1 has no concrete value yet.
//   s1 = d1 // Points to Dog but no data.
//   // s1 has a dynamic type but no dyanmic value.
//   // we can still call method Speak() of s1
//
// func (d *Dog) Speak(){
//    if d == nil{
//        fmt.Println("<noise>")
//    }else{
//        fmt.Println(d.name)
//    }
// }
//
// var s1 Speaker
// var d1 *Dog
// s1 = d1
// s1.Speak()
//
// Nil interface Value
//   interface with nil dynamic type
//   very differemt from an interface with a nill dynamic value.
// Nil dynamic value and valid dynamic type
// Without dynamic type, you cannot call the method Speak()
// var s1 Speaker
// var d1 *Dog
// s1 = d1
//
// If you have no dynamic type and no dynamic value on interface,
//   you can't call the methods of the interface.
//
// Usage of Interfaces.
// IF two types satisfy interface, they have common features.
// Need a function which takes multiple types of parameter:
//  Function foo() could take parameters Type X or Type Y
//  Define interface Z
//  foo() parameter can then be interfance Z
//  Make type X and Y satisfy Z
//  Interface methods must be those needed by foo()
// Interface generalizes the parameter types.
//
// Example: POOL IN A YARD:
//
// FitInYard() - should be able to take any shapes as argument.
//   - Takes a shape argument
//   - Returns true is shape satisfies the criteria
//   - the shapes types must have: Area() and Perimeter()
//
// type Shape2D interface{
//     Area() float64
//     Perimeter() float64
// }
//
// type Triangle {...}
// func (t Triangle) Area() float64{...}
// func (t Triangle) Perimeter() float64{...}
//
// type Rectangle {...}
// func (t Rectangle) Area() float64{...}
// func (t Rectangle) Perimeter float64{...}
//
// Rectangle and Triangle satisfy Shape2D interface.
//
// // Parameter is any type that satisfies the interface Shape2D
// func FitInYard(s Shape2D) bool{
//     if (s.Area() > 100) &&
//        s.Perimeter() > 100){
//            return true
//     }
//     return false
// }
//
//
// Empty Interface: specifies no methods.
// All types satisfy the empty interfance.
// Use it to have a function that accepts any type as a parameter.
//
// func PrintMe(val interface{}){
//     fmt.Println(val)
// }
// val can be any type and PrintMe will print it.
//
// The overall goal of interface is to conceal the differences between
//  types.
//
// Type Assertions
//   Sometimes we need to differentiate between types.
//   Figuring out the concrete type.
//
//  DrawShape() will draw shape
//  - func DrawShape() (s Shape2D){...}
//  inside DrawShape() we want to be able to disambiguate.
//
//  Underlying API has different drawing functions for each shape.
//  func DrawRect(r Rectangle){...}
//  func DrawTriangle(t Triangle){...}
//  To determine which function to call, we need to identify was type
//  s is by disambiguating the type inside DrawShape
//
//  Type Assertions for disambiguation:
//  type assertions can be used to determine and extract the
//  underlying concrete type.  
//
//  func DrawShape(s Shape2D) bool {
//      rect, ok := s.(Rectangle)
//      if ok {
//         DrawRect(rect)
//      }
//      tri, ok := s.(Triangle)
//      if ok{
//         DrawRect(tri)
//      }
//  }
//
//  Type assertion extracts Rectangle from Shape2D
//    - Concrete type in parentheses.
//  If interface contains concrete type
//    - rect == concrete type, ok == true
//  if interface does not contain concrete type
//    - rect == zero, ok == false
//
//  If you have multiple types, use Type Switch
//  
//  func DrawShape(s Shape2D) bool {
//       // sh is the actual s passed in
//    	 switch:= sh := s.(type){
//    	 case Rectangle:
//    	    DrawRect(sh)
//    	 case Triangle:
//    	       DrawRect(sh)
//      }
//  }
//
//
// Error handling (Error Interface)
//   many Go programs return error interface objects to indicates error
// type error interface{
//    Error() string
// }
//
// error == nil (everything is fine)
// Error() prints error message if something went wrong.
//
// Check error and handle to if need to.
//
//
//	QUIZE REVIEW
// 1. Polymorphism:
//

#---------------CONCURRENCY IN GO------------------------#

// CONCURRENCY AND PARALLELISM
//
// Concurrency is built into Go unline other programming languages
//   where is it not inbuilt. The other languages require libraries.
//
// Parallelism (Parallel Execution): Two programs execute in parallel
//   if they execute at exactly the same time.
//   One core runs one instruction at a time.
//   At time t, an instruction is being performed for both P1 and P2.
//   Running different instructions on different core.
//   In order to get parallelism, you need hardware replication.
//
// Why parallel execution:
//   - Better throughput: overall all tasks are completed more quickly.
//   - Tasks may complete more quickly.
//   - some tasks must be performed sequentially. (E.g. wash and dry)
//   - Some task are more parallelizable than other.
//   - More hardware is usually required.
//
// Concurrency
//  - Writing concurrent code is difficulty.
//  - Speedup without parallelism?
//  - Now, we need parallelism
//  - One way to do it, is to design faster processors (clock rate)
//  
// Von Neumann Bottleneck
//  - Delayed access to memory: grabbing data from the memory and
//  -  and writing back to memory.
//  - sometimes memory is slower even when clock rate is high.
//  - Waiting for memory for most times.
//  - Solution is to build cache on the chip to speed things up.
//  - Increasing on-chip cache improves performance.
//
// Moore's Law
//  - Transistor density would double every two years.
//  - If you can double transistor density, things speed up.
//  - The smaller the transistors, the faster they switch.
//  - Exponential increase in speed due to exponential increase
//    in density.
//  - Transistor density cannot continue to increase because
//  -  power consumption increases.
//
// Power/Temperature Problem
//  - The more transistor density, the more power consumption.
//  - Chips get heated up, fan is needed to dissipate heat.
//  - Temperature is the biggest limitation, the chip could be melted.
//  - Supercomputer has pipeline with cool agent.
//  - We can only dissipate much heat.
//  P = alpha * CFV^2
//  alpha: percent of time transistors are switching.
//  C: is capacitance (related to size)
//  F: clock frequency
//  V: Voltage swing (from low to high)
//  Voltage is important: 0 to 5V swing uses much more power than
//     0 to 1.3V
//  To save power, you reduce voltage.
//
// Dennard Scaling:
//  Voltage should scale with transistor size.
//  Keeps power consumption and temperature low.
//  Problem: Voltage can't go too low:
//    - Must stay above threshold voltage
//    - Noise problems occur
//    - There is a limit to voltage scaling.
//
// Multi-Core Systems
// P = alpha * CFV^2
// Cannot increase frequency
// Solution is to increase number of core
// Clock freq goes up slowly.
// To exploit multi-core systems, parallel execution is needed.
// Code made to execute on multiple cores
//    Different programs on different cores.
// Hence, concurrency is important these days:
//   putting different tasks on different core.
//
// Concurrent Execution:
//  - Not necessarily executing at the same time.
//  - start and end times overlap.
//  - Unlike parallel, they have to execute at the same time.
//
// Concurrent vs Parallel:
//   - Parallel tasks must be executed on different hardware
//   - Concurrent tasks may be executed on the same hardware
//     only one task actually executed at a time.
//   - Mapping from tasks to hardware is not directly controlled
//     by the programmer. In some languages, programmer could map
//     core to each task.
//     Not in Go.
//
// Programmer can determine which tasks can be executed in parallel.
// Operating system handles mapping tasks to hardware
// Go runtime scheduler also decides that.
//
// Even if you can't get parallelism.
// Concurrency improves performance even without parallelism
// Task must periodically wait for something:
//     - wait for memory.
// Other concurrent tasks can operate/execute while one task is
//   waiting to access data from memory. This leads to hiding latency.
//   Taking another task to run while waiting for another task to
//   finish; waiting for I/O task.
// Hardware Mapping:
//  - Programmer does not determine the hardware mapping
//  - Understanding underlying hardware architecture can be hard.
//  - Could make programmer's job difficult. 

// Processes
// Concurrency
// Process: instance of a running program
//   - Every process has a memory (virtual address) space.
//   - stack, code, and heap are unique to a process
//   - Registers: program counter, data registers, and stack ptr.
// Every process has a unique context (bunch of memory and register values unique to the process.
// Every process is an executing or running program.

// Operating system(O/S): allows many processes to execute concurrently
//    processes should be able to access memory without bumping into
//    each other.
//    - Each process should get fair used of the processor.
//    - Processes are switched quickly: typically every 20ms
//       20ms could changed. It seems all processes are running
//       at the same time.
//    - Also called Scheduler.
//    - OS must give processes fair access to resources (memory,
//      I/O, display etc.)
//    - managing a pile of processes without interfering with each
//      other and get fair access to resources.

// Scheduling Processes
// OS schedules processes for execution
// Gives the illusion of parallel execution
// There are many scheduling algorithms.
//   Round robin is an example.
// OS gives fair access to CPU, memory, etc.
// There is room for prioritizing process.
// Scheduling is the main task in the operating system.
// Context switch: control flow changes from one process to another
//    current state (context) of a process is saved before switching
//    to another process, so that we can pick up from where we stopped.
//    context includes memory, virtual address system, etc.
//    OS performs the context swtich.
//    Context switch is the kernel of an OS.

// Threads and Goroutine
// Thread vs Processes:
//   In Processes: context switching between processes could be slow.
//   Thread (originally caused light weight processes): shares a 
//     decent amount of context.
//   Process could consists of many threads.
//   There is unique context for each thread, but they also share
//     context such as virtual memory and file descriptors.
// Context switching between processes could take longer than
//    switching between threads.
// Thread-level granuality.

// Goroutines
// Basically a thread in Go.
// Operates in a single thread.
// Many Goroutines execute within a single OS thread.
// Operating system schedule a single main thread.
// Go switches the goroutine inside the main thread.
//    THis is done by Go Runtime Scheduler
// Go runtime scheduler
//   - schedules goroutines inside an OS thread
//   - Like as little OS inside a single OS thread
//   - Logical processors is mapped to a thread.
// As a programmer, you can request multiple logical processor.
//  - map each main thread (containing one goroutine) to each
//    logical process. For OS to do parallel scheduling.

// Interleavings
// Having mental model of the state of the program at any time
// makes writing concurrent code difficult.
// With concurrent code, it's hard to tell the overall state of the
//   machine. Machine state is not deterministic.
// Interleaving of instructions:
//   Order of execution within a task is known.
//   Order of execution between concurrent tasks is unknown.
//   Instructions might interleave
//   The interleave is not deterministic, changes with every runs.
//   Many interleavings are possible and must consider all possibility
//   Ordering is non-deterministic
//   Interleaving can even happened inside each instruction.

// Race conditions
// Occur due to interleaving.
// Outcome of the program depends on the non-deterministc ordering.
// Race condition occur due to communication between tasks or gorountines.
// Communication on variable x for example.
// Threads are largely independent (mostly no communication needed)
//   but not completely independent.
//   threads share common resources inside a process.
//   Webserver is an example of multithreaded system.
//      one thread per client.
//   Threads are independent but could share data sometimes.
//   Periodically, they communicate.
// Use multiple goroutines if frequent communication is needed.

// Assume that two goroutines are executed concurrently, the relative
//   order of the execution of the their instructions can be different
//   everytime that they are executed together.
// Without synchronization, the relation completion times between two
//   goroutines is unknown.
// Go runtime scheduler schedules goroutines inside an OS thread.
// Thread vs Process: thread has less unique context than a process.
// Context: in context switch, context means memory and register
//   values unique to a process.
// OS enables concurrency on a single processor machine using interleaves the execution of differen processes.
// Process unique resources: program counter value and virtual address space.
//
//
// Threads in Go (goroutines)
//
// Creating a goroutine
// One goroutine is created automatically to execute the main()
// a = 1
// foo()
// a = 2
// a = 1 executes before foo() gets executed and a = 2 executes after
// foo() is completed. 
//
// Create a new goroutine:
// a = 1
// go foo()
// a = 2
// a = 2 can execute immediately and doesn't have to wait for go foo()
// fo finish.
//
// A goroutine exists when its comde is complete.
// When the main goroutine is complete, all other goroutines within
// the main goroutine are forced to exist.
//
// Existing goroutine:
// func main(){
//     go fmt.Printf("New routine")
//     fmt.Printf("Main routine")
// }
// Only Main routine is printed. This is because Main routine finished
// before the new goroutine started.
//
// Solution: Delayed Exit - to prevent Main from existing
//
// func main(){
//     go fmt.Printf("New routine")
//     time.Sleep(100 * time.Millisecond)
//     fmt.Printf("Main	  routine")
// }
// Delays the main routine and in that waiting or blocking time,
// so that scheduler can schedule the New routine to execute.
//
// Downside of induced delay:
//   - Adding a delay to wait for a goroutine is bad!
//   - Timing assumptions may be wrong.
//   - Operating might use the waiting time 100ms to schedule
//     a different thread completely different from what the 
//     goroutine it is expected to execute.
//   Timing is nondeterministic.
// Timing is not reliable, so we need a formal synchronization constructs.
//
// Basic Synchronization
//  - Multiple threads agree on the timing of a particular event.
//  - Using global events whose execution is viewed by all threads
//    simultaneously.
//  - Help reduce interleaving that can happen.
//  - To prevent race condition, we need a global event that all
//  - threads can see.
//  - With synchronization, we are reducing efficiency or performance.
//
// Sync WaitGroup
// - type of synchronization
// - contained in sync package.
// - Forces a goroutine to wait for other goroutines.
// - The waiting goroutine will not execute until the other goroutines
//   in WaitGroup finished.
// - sync.WaitGroup: contains counting semaphore (internal counter)
//   - Increment counter for each goroutine to wait for
//   - Decrement counter for each goroutine thats completed.
//   - Waiting goroutine cannot continue until counter is 0.
/*
    var wg sync.WaitGroup
    wg.Add(1)
    go foo(&wg)
    wg.Wait() // the Main thread is blocked until foo finished
    wg.Done() // foo calls wg.Done to indicate its done.
*/

// Add()  - increments the counter.
// Done() - decrements the counter.
// Wait() - blocks until counter == 0
// Each goroutine (thread) must call wg.Done() when they are done.
// wg.Wait() must be called in the main routine.

func foo(wg *sync.WaitGroup){
    fmt.Printf("New routine")
    wg.Done()
}

func main(){
    var wg sync.WaitGroup
    wg.Add(1)
    go foo(&wg)
    wg.Wait()
    fmt.Printf("Main routine")
}


// COMMUNICATIONS
// - goroutine also communicate at times.
// - because goroutines are working together to complete a big task.
// - Common to make a webserver: each connection is assigned a thread.
// - threads are sharing data - interactions between threads
// - goroutines need to receive and send data.
// - send data to collaborate.
//
// Channels are used to to communicate.
// - Channels are typed.
// - Use make() to create a channel:
//   c := make(chan int) // Send int
//   Sending: c <- 3
//   Receiving x := <- c
//
//  func prod(v1 int, v2 int, c chan int){
//      c <- v1 * v2
//  }
//
//  func main(){
//     c := make(chan int)
//     go prod(1,2,c)
//     go prod(3,4,c)
//     a := <- c
//     b := <- c
//     fmt.Println(a*b)
//  }
//
//
// Blocking in Channels
// Unbuffered Channel cannot hold data in transit.
//  - Default is unbuffered
// Sending blocks until data is received
// Receiving blocks until data is sent.
// Send will block until receive happens
//  and vice versa because there is no buffering.
// You have to wait so you don't lose datum
//  since it is blocking channel.
// Channel communication is synchronouse
// Blocking is the same as waiting for communication.
// Channel can also be used as waiting strategy to sync
// goroutines - another way to implement waiting achieved
// WaitGroup. Hence, sync is also built into the communication
//   construct.
//
// Buffered Channel:
// Unbuffered channel is the default without capacity to hold data.
// - Default size 0 (unbuffered)
// Capacity is the number of objects it can hold in transit.
// To create channel with capacity:
// c := make(chan int, 3) // Add optional argument to define capacity.
// Sending and Receiving Block now happens on certain conditions.
//    - Sending only blocks if buffer is full.
//    - Can do 3 sends with no blocking.
// Blocking is a bad thing and reduce concurrency.
//   - wasting processor resources.
// Having a channel capacity means you can continue until buffer's full
// Although, channel capacity is limited because of memory is finite.
// Receiving only blocks if buffer is empty.
//   - receiving continues until nothing left in the buffer.
//
// Why use buffering:
//   - sender and receiver do not need to operate at exactly the same
//     speed.
//   - avoids the use of blocking, which reduces concurrency
//   - Speed mismatch is acceptable.
//   - On average speed has to match to prevent buffer overflow.