package main

import (
        "fmt"
        "os"
        "bufio"
        "strings"
)

// Interface type
type Animal interface{
    Eat()
    Move()
    Speak()
}

// Animal types
type Cow struct {
    food string
    movement string
    noise string
}

type Bird struct{
    food string
    movement string
    noise string
}

type Snake struct{
    food string
    movement string
    noise string
}

// Make Cow satisfies Animal interface
func (cw Cow) Eat(){
    fmt.Println(cw.food)
}

func (cw Cow) Move(){
    fmt.Println(cw.movement)
}

func (cw Cow) Speak(){
    fmt.Println(cw.noise)
}

// Make Snake satisfies Animal interface
func (sn Snake) Eat(){
    fmt.Println(sn.food)
}

func (sn Snake) Move(){
    fmt.Println(sn.movement)
}

func (sn Snake) Speak(){
    fmt.Println(sn.noise)
}

// Make Bird satisfy Animal interface
func (brd Bird) Eat(){
    fmt.Println(brd.food)
}

func (brd Bird) Move(){
    fmt.Println(brd.movement)
}

func (brd Bird) Speak(){
    fmt.Println(brd.noise)
}

func main(){
  fmt.Println("----------------------------------------------------------------")
  fmt.Println(" WELCOME TO ANINAL KINGDOM, ENTER A COMMAND: newanimal or query ")
  fmt.Println("----------------------------------------------------------------")
  // map to store animal set. Animal's name is the unique key.
  var animalSet = make(map[string] Animal)

  for{
     fmt.Println("WAITING FOR COMMAND ")
     fmt.Printf(" > ")
     userInputReader := bufio.NewReader(os.Stdin)
     inputValues, _ := userInputReader.ReadString('\n')
     inputValues = strings.TrimSpace(inputValues)
     inputVals := strings.Split(inputValues, " ")

     // Catch wrong user input - user inputs must 3 strings
     if (len(inputVals) < 3 || len(inputVals) > 3) {
         fmt.Println("ERROR PROCESSING YOUR COMMAND")
         fmt.Println("Enter your command and other information separated by space.")
         fmt.Println("")
         continue
     }
     
     // Animal kingdom command.
     firstInput := strings.ToLower(inputVals[0])
     // Name of animal
     secondInput   := strings.ToLower(inputVals[1])
     // Type of animal to clone.    
     thirdInput := strings.ToLower(inputVals[2])

     // Create the new animal clone.
     if (strings.Compare(firstInput, "newanimal")==0){
         // Check animal type is correct and the new animal name doesn't exist yet.
         _, ok := animalSet[secondInput]
         if (strings.Compare(thirdInput, "cow")==0 && !ok){
      	     newAnimal := Cow{"grass", "walk", "moo"}
             animalSet[secondInput] = newAnimal
             fmt.Println("Created it!")
         }else if (strings.Compare(thirdInput, "bird")==0 && !ok){
             newAnimal := Bird{"worms", "fly", "peep"}
             animalSet[secondInput] = newAnimal
             fmt.Println("Created it!")
         }else if(strings.Compare(thirdInput, "snake")==0 && !ok){
             newAnimal := Snake{"mice", "slither", "hsss"}
             animalSet[secondInput] = newAnimal
             fmt.Println("Created it!")
         } else {
             fmt.Println("Animal name already exists or wrong animal type specified.")
             fmt.Println("ERROR!: Types can only be Cow, Bird, or Snake")
         }
     }else if (strings.Compare(firstInput, "query")==0){
         // Process Query Commands.
         // Check if the animal requested has been created.
         anim, ok := animalSet[secondInput]
         if ok{
             // Animal exists
             switch anim.(type){
                 case Cow:
                     if (strings.Compare(thirdInput, "eat")==0){
                         anim.Eat()
                     }else if (strings.Compare(thirdInput, "move")==0){
                         anim.Move()
                     }else if (strings.Compare(thirdInput, "speak")==0){
                         anim.Speak()
                     }else{
                         fmt.Println("Unknown Information Requested! Try Again!")
                     }
              	 case Bird:
	             if	(strings.Compare(thirdInput, "eat")==0){
                         anim.Eat()
                     }else if (strings.Compare(thirdInput, "move")==0){
                      	 anim.Move()
                     }else if (strings.Compare(thirdInput, "speak")==0){
                      	 anim.Speak()
                     }else{
			 fmt.Println("Unknown Information Requested! Try Again!")
                     }
                 case Snake:
                     if (strings.Compare(thirdInput, "eat")==0){
                         anim.Eat()
                     }else if (strings.Compare(thirdInput, "move")==0){
                         anim.Move()
                     }else if (strings.Compare(thirdInput, "speak")==0){
                         anim.Speak()
                     }else{
                         fmt.Println("Unknown Information Requested! Try Again!")
                     }
                 default:
                     fmt.Println("Unknown Animal Type Requested!")
             }
         }else{
             // Unknown Animal.
             fmt.Println("Unknown Animal Name, Make sure it's created.")
             fmt.Println("Requested Animal name: ", secondInput)
         }
     }else{
         fmt.Println("Unknown Command: ", firstInput)
     }
  }   
}