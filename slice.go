package main

import (
         "fmt"
         "sort"
         "strings"
         "bufio"
         "os"
         "strconv"
)

func main(){
   // Integer variable to store user input.
   var userNumber int
   // Slice to store user input.
   userSliceInput := make([]int, 0, 3)

   for{
        // User enters new integer
        fmt.Printf("Please enter an integer or 'X' to quit > ")
        userInput := bufio.NewReader(os.Stdin)
        userIn, _ := userInput.ReadString('\n')
        userIn = strings.TrimSpace(userIn)

        if 0 == strings.Compare(userIn, "X") {
            // Sort the current slice
      	    sort.Ints(userSliceInput)
            fmt.Println("STOPPED")
            // Print existing sorted slice
            fmt.Println("Current Sorted Slice: ", userSliceInput)
            fmt.Println("Have a nice day!")
            break
        }
        // String to integer
        userNumber, _ = strconv.Atoi(userIn) 
        // Add new integer to slice.
        userSliceInput = append(userSliceInput, userNumber)
        // Print sorted list
        sort.Ints(userSliceInput)
        fmt.Println("Sorted Slice: ", userSliceInput)
   }
}