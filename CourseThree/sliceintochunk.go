package main

import (
        "fmt"
        "os"
        "bufio"
        "strings"
        "strconv"
)

// Create chunks
func PartitionArray(inputSeq []int, numGoRtn int)([][]int, error){
    // Partitioned list
    partitionedArray := [][]int{}
    arrSize := len(inputSeq)
    // Catch edge case.
    if numGoRtn == 0 || numGoRtn > arrSize{
        return partitionedArray, fmt.Errorf("Not able to split array size %d int %d partitions", arrSize, numGoRtn)
    }

    partitionSize := arrSize / numGoRtn
    for k, n := 0, 0; k < numGoRtn; k, n = k+1, n+partitionSize{
        val := inputSeq[n : n+partitionSize]
        if k == numGoRtn - 1{
            val = inputSeq[n:]
        }
        partitionedArray = append(partitionedArray, val)
    }
    return partitionedArray, nil
}

func main(){
        // Get series of integers from user.
    fmt.Println("Enter sequence integers separated by space.")
    fmt.Printf(" > ")
    userInputReader := bufio.NewReader(os.Stdin)
    inputSequence, _ := userInputReader.ReadString('\n')
    inputSequence = strings.TrimSpace(inputSequence)

    // Split the input separated by whitespace.
    inputSeq := strings.Split(inputSequence, " ")
    sliceInputIntSeq := make([]int, 0, len(inputSeq))

    // Process user input and add each integer to slice.
    for k := range inputSeq{
       intVal, _ := strconv.Atoi(inputSeq[k])
       sliceInputIntSeq = append(sliceInputIntSeq, intVal)
    }

    // Number of threads (goroutines)
    numGoRtn := 4
    // Partitioned Array
    partitionedData, ok := PartitionArray(sliceInputIntSeq, numGoRtn)
    if ok != nil{
        fmt.Println(ok)
        fmt.Println("Please ensure you entered at least %d integers", numGoRtn)
    }
    fmt.Println(partitionedData)
}