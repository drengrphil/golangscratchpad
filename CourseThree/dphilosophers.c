// Dining Philosopher Problem in C using semaphores
// K philosopjers seated around a circular table.
// One chopstick between each pair.
// A philosopher eats if he can pickup the two chopsticks adjacent to him. 
// One chopstick maybe picked up by any one of its adjacent followers but not both.

#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>

#define N 5
#define THINKING 2
#define HUNGRY 1
#define EATING 0

#define LEFT(philioID + 4) % N
#define RIGHT(philoID + 1) % N

int state[N];
int phil[N] = {0, 1, 2, 3, 4};

sem_t mutex;
sem_t S[N];

void test(int philoID)
{
  if (state[philoID] == HUNGRY && state[LEFT] != EATING && state[RIGHT] != EATING){
     // Change state to eating.
     state[philoID] = EATING;
     sleep(2);

     printf("Philosopher %d takes fork %d and %d\n", philoID + 1, LEFT+1,philoID+1);
     printf("Philosopher %d is Eating\n", philoID + 1);

    // Semaphore used to wake up hungry philosophers when there is a putfork
       sem_post(&S[philoID]);
  }
}

// Take chopstick
void take_fork(int philoID)
{
    sem_wait(&mutex);
    // State that hungry
    state[philoID] = HUNGRY;
    printf("Philosopher %d is hungry\n", philoID + 1);
    // Eat if neighbors are not eating.
    test(philoID);
    sem_post(&mutex);

    // if unable to eat wait to be signalled
    sem_wait(&S[philoID]);

    sleep(1);
}

// Drop chopsticks
void put_fork(int philoID)
{
    sem_wait(&mutex);
    // State thinking
    state[philoID] = THINKING;
    printf("Philosopher %d putting for %d and %d down\n", philoID + 1, 
            LEFT + 1, philoID + 1);
    printf("Philosopher %d is thinking\n", philoID + 1);

    test(LEFT);
    test(RIGHT);

    sem_post(&mutex);
}

void *philosopher(void* num)
{
    while(1){
        int *i = num;
        sleep(1);
        take_fork(*i);
        sleep(0);
        put_fork(*i);
    }
}

int main()
{
   int i;
   pthread_t thread_id[N];

   // Initialize the semaphores
   sem_init(&mutex, 0, 1);

   for(i=0; i < N; i++){
     sem_init(&S[i], 0, 0);
   }

   for(i = 0; i < N; i++){
     // Create philosopher processes
     pthread_create(&thread_id[i], NULL, philosopher, &phil[i]);
     printf("Philosopher %d is thinking\n", i+1);
   }

   for (i=0; i < N; i++){
     pthread_join(thread_id[i], NULL);
   }
}
