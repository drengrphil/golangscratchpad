//  Write a program to sort an array of integers. The program should 
//  partition the array into 4 parts, each of which is sorted by a 
//  different goroutine. Each partition should be of approximately 
//  equal size. Then the main goroutine should merge the 4 sorted 
//  subarrays into one large sorted array.

//  The program should prompt the user to input a series of integers. 
//  Each goroutine which sorts ¼ of the array should print the subarray 
//  that it will sort. When sorting is complete, the main goroutine should 
//  print the entire sorted list.

package main

import (
    "fmt"
    "sync"
    "os"
    "bufio"
    "strings"
    "strconv"
)

// Swap operation to swap position of two adjacent
// elements in the slice.
func Swap(userDigits []int, indx int){
    tempPos := userDigits[indx]
    userDigits[indx] = userDigits[indx+1]
    userDigits[indx+1] = tempPos
}

// Bubble sort function to sort slice of integers
// in order.
func BubbleSort(userDigits []int){
    for k := 0; k < len(userDigits)-1; k++{
        for j := 0; j < len(userDigits)-k-1; j++{
            if userDigits[j] > userDigits[j+1]{
                Swap(userDigits, j)
            }
        }
    }
}

func SortRequest(userDigits []int, wg *sync.WaitGroup, thrdID int){
     fmt.Printf("Thread %d sorting %d\n", thrdID, userDigits)
     BubbleSort(userDigits)
     wg.Done()
}

// Function to partition integer sequence among goroutines (threads).
func PartitionArray(inputSeq []int, numGoRtn int)([][]int, error){
    // Partitioned list
    partitionedArray := [][]int{}
    arrSize := len(inputSeq)
    // Catch edge case.
    if numGoRtn == 0 || numGoRtn > arrSize{
        return partitionedArray, fmt.Errorf("Not able to split array size %d int %d partitions", arrSize, numGoRtn)
    }

    partitionSize := arrSize / numGoRtn
    for k, n := 0, 0; k < numGoRtn; k, n = k+1, n+partitionSize{
        val := inputSeq[n : n+partitionSize]
        if k == numGoRtn - 1{
            val = inputSeq[n:]
        }
        partitionedArray = append(partitionedArray, val)
    }
    return partitionedArray, nil
}

// Merge all list from goroutines
func MergeSortedList(SortedPartitionData [][]int, numElem int){
      finalSortedList := make([]int, 0, numElem)
      // For each subarray
      for k := len(SortedPartitionData)-1; k >=0; k-- {
          // fmt.Println(SortedPartitionData[k])
          for q := len(SortedPartitionData[k])-1; q >= 0; q--{
                finalSortedList = append(finalSortedList, SortedPartitionData[k][q])
          }
      }
      // Sorted merged list:
      fmt.Println("Sorted Merged List:")
      BubbleSort(finalSortedList)
      fmt.Println(finalSortedList)
}

func main(){
    // Get series of integers from user.
    fmt.Println("Enter sequence integers separated by space.")
    fmt.Printf(" > ")
    userInputReader := bufio.NewReader(os.Stdin)
    inputSequence, _ := userInputReader.ReadString('\n')
    inputSequence = strings.TrimSpace(inputSequence)

    // Split the input separated by whitespace.
    inputSeq := strings.Split(inputSequence, " ")
    sliceInputIntSeq := make([]int, 0, len(inputSeq))

    // Process user input and add each integer to slice.
    for k := range inputSeq{
       intVal, _ := strconv.Atoi(inputSeq[k])
       sliceInputIntSeq = append(sliceInputIntSeq, intVal)
    }

    // Number of threads (goroutines)
    numGoRtn := 4
    // Partitioned Array amoung goroutines
    partitionedData, ok := PartitionArray(sliceInputIntSeq, numGoRtn)
    if ok != nil{
        fmt.Println(ok)
      	fmt.Println("Please ensure you entered at least %d integers", numGoRtn)
    }
    
    fmt.Println("Unsorted Partitioned Data:")
    fmt.Println(partitionedData)
    
    var wg sync.WaitGroup
    wg.Add(1)
    go SortRequest(partitionedData[0], &wg, 1)
    wg.Add(1)
    go SortRequest(partitionedData[1], &wg, 2)
    wg.Add(1)
    go SortRequest(partitionedData[2], &wg, 3)
    wg.Add(1)
    go SortRequest(partitionedData[3], &wg, 4)
    wg.Wait()
    fmt.Println("All goroutines finished sorting")
    fmt.Println("Sorted Lists from threads:")
    fmt.Println(partitionedData)
    MergeSortedList(partitionedData, len(sliceInputIntSeq))
}