package main

import (
    "fmt"
    "time"
)

/*
 * Explain what the race condition is and how it can occur:
 *
 * The following program defines a map that stores the name of animals.
 * And different goroutines are forked to write to the map (AnimalMap)
 * Each goroutine will attempt to rename each name stored in the map.
 * Race condition occurs when two or more goroutines try to modify the
 * same memory location without any kind of synchronization between them.
 * For instance, lets say we have animal name "cowboy" in AnimalMap and
 * goroutine G_1 wants to change "cowboy" to "cowman" while goroutine
 * G_2 wants to change "cowboy" to "cowfriend", that will lead to 
 * race condition and inconsistency in data stored in AnimalMap.
 * The lines at the of this file shows a runtime error reporting
 * the occurrence of race condition. Such race condition occurs
 * during some run while it doens't happen some times.
 */

// Map for in-memory representation of set of animal names.
var AnimalMap = make(map[int] string)

// Function to register animal names in AnimalMap
// This function just populate the map.
func animalRegistrar(){
    AnimalMap[0] = "cowboy"
    AnimalMap[1] = "goatboy"
    AnimalMap[2] = "chickenman" 
}

// Function to modify animal names stored in AnimalMap
func changeAnimalName(newName string, key int, gortn int){
    AnimalMap[key] = newName
    fmt.Println("goroutine ", gortn)
    fmt.Println(AnimalMap[key])
}

func main(){
    // goroutine 1 will change AnimalMap[0]
    go changeAnimalName("cowman", 0, 1)

    // goroutine 2
    go changeAnimalName("cowfriend", 0, 2)
   
    time.Sleep(time.Second)
    fmt.Println("Finished")
}
// End of executed code. 


/*
fatal error: concurrent map writes

goroutine 7 [running]:
runtime.throw(0x10cfd1d, 0x15)
			 /usr/local/go/src/runtime/panic.go:1116 +0x72 fp=0xc000032f00 sp=0xc000032ed0 pc=0x102eb12
runtime.mapassign_fast64(0x10b2c00, 0xc000056180, 0x0, 0x0)
				    /usr/local/go/src/runtime/map_fast64.go:101 +0x323 fp=0xc000032f40 sp=0xc000032f00 pc=0x100f973
main.changeAnimalName(0x10cdead, 0x9, 0x0, 0x2)
				 /Users/phil/GoLearn/golangscratchpad/CourseThree/goroutinetask.go:35 +0x4a fp=0xc000032fc0 sp=0xc000032f40 pc=0x109d50a
runtime.goexit()
	/usr/local/go/src/runtime/asm_amd64.s:1373 +0x1 fp=0xc000032fc8 sp=0xc000032fc0 pc=0x105b7d1
created by main.main
	/Users/phil/GoLearn/golangscratchpad/CourseThree/goroutinetask.go:45 +0x9f

goroutine 1 [sleep]:
time.Sleep(0x3b9aca00)
	/usr/local/go/src/runtime/time.go:188 +0xba
main.main()
	/Users/phil/GoLearn/golangscratchpad/CourseThree/goroutinetask.go:47 +0xac

goroutine 6 [runnable]:
fmt.(*pp).doPrintln(0xc0001000d0, 0xc00005cf90, 0x2, 0x2)
				  /usr/local/go/src/fmt/print.go:1168 +0x1df
fmt.Fprintln(0x10eac40, 0xc00000e018, 0xc00005cf90, 0x2, 0x2, 0x0, 0x0, 0x0)
			/usr/local/go/src/fmt/print.go:264 +0x58
fmt.Println(...)
	/usr/local/go/src/fmt/print.go:274
main.changeAnimalName(0x10cd8c0, 0x6, 0x0, 0x1)
				 /Users/phil/GoLearn/golangscratchpad/CourseThree/goroutinetask.go:36 +0xf7
created by main.main
	/Users/phil/GoLearn/golangscratchpad/CourseThree/goroutinetask.go:42 +0x60
*/
