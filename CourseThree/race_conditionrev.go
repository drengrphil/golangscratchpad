package main

import (
	"fmt"
	"time"
)

func print(text string) {
	fmt.Println(text)
}

func main() {
	go print("00")
	go print("99")

	time.Sleep(time.Second)
	fmt.Println("done")
}

/*
This program creates 2 gorutines that print 99 or 00. 
Each time we run this progam we don't know which value will be printed before the other.
The one that eventually finish before will win. That's a race condition.
*/
