// Testing goroutine.

package main

import (
    "fmt"
    "time"
)

func f(from string)){
    for i := 0; i < 3; i++{
        fmt.Println(from, ":", i)
    }
}


func main(){
    // Normal call without goroutine
    f("direct")
    
    // Invoke same function in goroutine
    // It should execute concurrently with the normal call above.
    go f("goroutine")

    // Start another goroutine for annoymous function.
    go func(msg string){
        fmt.Println(msg)
    }("going")

   // Above we have two functions running asynchronously in separate
   // goroutines now. Wait for them to finish.
   // For more robust approach, use WaitGroup
   time.Sleep(time.Second)
   fmt.Println("done")
}