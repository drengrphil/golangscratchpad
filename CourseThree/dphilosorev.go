package main

import (
	"fmt"
	"sync"
)

type ChopS struct{ sync.Mutex }

type Philo struct{ leftCS, rightCS *ChopS }

var wg sync.WaitGroup

func (p Philo) eat(host Host, index int) {
	for i := 0; i < 3; {
		if host.ask() {
			p.leftCS.Lock()
			p.rightCS.Lock()
			fmt.Print("starting to eat ", index, "\n")
			host.free()
			fmt.Print("finishing to eat ", index, "\n")
			p.rightCS.Unlock()
			p.leftCS.Unlock()
			i++
		}
	}
	wg.Done()
}

type Host struct{ count int }

func (h Host) ask() bool {
	if h.count > 0 {
		h.count--
		return true
	}
	return false
}
func (h Host) free() {
	h.count++
	if h.count > 2 {
		h.count = 2
	}
}
func main() {
	CSticks := make([]*ChopS, 5)
	for i := 0; i < 5; i++ {
		CSticks[i] = new(ChopS)
	}

	philos := make([]*Philo, 5)
	for i := 0; i < 5; i++ {
		philos[i] = &Philo{CSticks[i], CSticks[(i+1)%5]}
	}

	wg.Add(5)
	host := Host{2}
	for i := 0; i < 5; i++ {
		go philos[i].eat(host, i)
	}
	wg.Wait()
}
