// Implement the dining philosopher's problem under the following constraints:
// Five philosophers sharing chopsticks, with one chopstick between each adjacent pair of philosophers.
// Each philosopher should eat only 3 times (not infinite loop)
// The philosophers pick up the chopsticks in any order.
// In order to eat, a philosopher must get permission from a host which executes in its own goroutine
// The host allows no more than 2 philosophers to eat concurrently.
// Each philosopher is numbered, 1 through 5
// When a philosopher starts eating (after holding proper locks) it prints "starting to eat <number>"
// <number> is the number of the philosopher.
// When a philosopher finishes eating (before releasing locks), it prints "Finishing eating<number>"

package main

import (
        "fmt"
        "sync"
        "time"
)

// Wait Group
var wg sync.WaitGroup

// Chopsticks
type ChopSticks struct{
    sync.Mutex
}

// Philosophers
type Philosopher struct{
    LeftChopStick  *ChopSticks
    RightChopStick *ChopSticks
}

func (philo Philosopher) EatDinner(PhiloID int, wg* sync.WaitGroup){
    // Each philosopher should only eat three times
    for k := 0; k < 3; k++{
        philo.LeftChopStick.Lock()
        philo.RightChopStick.Lock()
        fmt.Printf("Philosopher %d starting to eat.\n", PhiloID)
        
        time.Sleep(time.Second)

        fmt.Printf("Philosopher %d finished eating.\n", PhiloID)

        // Release locks on the chopstick
        philo.LeftChopStick.Unlock()
        philo.RightChopStick.Unlock()
    }
    wg.Done()
}

func main(){
    
    // Number of philosopher at dinning
    numPhilosophers := 5

    // Chopsticks for philosophers
    PhiloChopSticks := make([] *ChopSticks, numPhilosophers)
    for i := 0; i < numPhilosophers; i++{
         PhiloChopSticks[i] = new(ChopSticks)
    }

    // Create philosophers
    FivePhilosophers := make([] *Philosopher, numPhilosophers)
    for k := 0; k < numPhilosophers; k++{
        FivePhilosophers[k] = &Philosopher{PhiloChopSticks[k], PhiloChopSticks[(k+1)%numPhilosophers]}
    }

    // Threads for each philosopher
    for i := 0; i < numPhilosophers; i++{
         wg.Add(1)
         go FivePhilosophers[i].EatDinner(i, &wg)        
    }
    
    // Cause main goroutine to wait for the philosophers threads.
    wg.Wait()
}


