package main

import (
        "fmt"
        "os"
        "strings"
        "bufio"
        "io"
)

func main(){
    
    // Struct to store names found in file.
    type name struct{
        fname string
        lname string
    }

    // Slice for name struct.
    sliceNameList := make([]name, 0, 3)

    // Enter the name of file to process.
    fmt.Printf("Enter File Name > ")
    userFileRd := bufio.NewReader(os.Stdin)
    fileName, _ := userFileRd.ReadString('\n')
    fileName = strings.TrimSpace(fileName)

    // Open file fileName for reading.
    fd, err := os.Open(fileName)
    if err != nil{
        fmt.Println("Failed to Open file")
        return
    }

    // Fetched bytes from file.
    fdReader := bufio.NewReader(fd)
    for{
           nByte, err := fdReader.ReadString('\n')
           fName := new(name)
           if err == io.EOF{
               fmt.Println("Finished Reading File.")
               fd.Close()
               break
           }
           
           // Split line by space.
           fullName2 := strings.Split(nByte, " ")
           fName.fname = fullName2[0]
           fName.lname = fullName2[1]           
           // Add full name to slice.
           sliceNameList = append(sliceNameList, *fName)
    }

    // Print slice contents
    fmt.Println("=== ========== =========")
    fmt.Println("S/N First-Name Last-Name")
    fmt.Println("=== ========== =========")
    for ind, nam := range sliceNameList{
        fmt.Println(ind, " ", nam.fname, " ", nam.lname)
    }
}