package main
import "fmt"
func main(){
    // Float variable to store user input.
    var userNumber float64
    // Wait for user input.
    fmt.Printf("Please enter a float value > ")
    fmt.Scan(&userNumber)    
    
    // Convert float to integer
    var userIntNum int = int(userNumber)
    fmt.Println("Approximated Value from User:", userIntNum)
}